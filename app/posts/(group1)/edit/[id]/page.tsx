export default function Page({ params }: { params: { id: string } }) {
    return <div>Edit post {params.id}</div>
}