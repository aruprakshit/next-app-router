export default function Page({ params }: { params: { id: string } }) {
    if (params.id === "1") {
        throw new Error("Not found") ;
    }
    return <div>post #{params.id}</div>
}