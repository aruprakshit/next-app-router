export default function Layout({
    children,
}: {
    children: React.ReactNode
}) {
    return (
        <div style={{ backgroundColor: "blue" }}>
            {children}
        </div>
    )
}