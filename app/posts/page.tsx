import Link from 'next/link'
 
export default function posts({ postList }) {
  return (
    <ul>
      {postList.map((post) => (
        <li key={post.id}>
          <Link href={`/blog/${post.slug}`}>{post.title}</Link>
        </li>
      ))}
    </ul>
  )
}