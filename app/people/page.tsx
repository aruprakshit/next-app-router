async function getData() {
    const res = await fetch('https://swapi.dev/api/people')

    if (!res.ok) {
        // This will activate the closest `error.js` Error Boundary
        throw new Error('Failed to fetch data')
    }

    return res.json()
}

export default async function Page() {
    const data = await getData()

    return <ul>{
        data.results.map((person: any) => <li key={person.name}>{person.name}</li>)
    }</ul>
}
