'use client'

import { useRouter } from 'next/navigation'

import Link from 'next/link'

export default function Home() {
  const router = useRouter()

  return (
    <div>
      <h1>Home</h1>
      <Link href="/posts/1">Go to Post One</Link>
      <button type="button" onClick={() => router.push('/about')}>
        About
      </button>
    </div>
  )
}